const Item = require('../src/item.js').Item;
const assert = require('chai').assert;


describe('Item', function() {
    let milk 
    let bread

    before(() => {
        milk = new Item('Milk', 2.50, 3);
        bread = new Item('Bread', 2.00, 1);
    });

    describe('#getName()', function() {
        it('returns a string', function(done) {
            assert.typeOf(milk.getName(), 'string');
            assert.typeOf(bread.getName(), 'string');
            done();
        });
        it('returns the correct value', function(done) {
            assert.equal(milk.getName(), 'Milk');
            assert.equal(bread.getName(), 'Bread');
            done();
        });
    });
    describe('#getPrice()', function() {
        it('returns a number', function(done) {
            assert.isNumber(milk.getPrice());
            assert.isNumber(bread.getPrice());
            done();
        });
        it('returns the correct value', function(done) {
            assert.equal(milk.getPrice(), 2.50);
            assert.equal(bread.getPrice(), 2.00);
            done();
        });
    }); 
    describe('#setPrice()', function() {
        it('sets the price to the correct value', function(done) {
            milk.setPrice(3.00);
            bread.setPrice(2.25);
            assert.equal(milk.getPrice(), 3.00);
            assert.equal(bread.getPrice(), 2.25);
            done();
        });
    });       
});