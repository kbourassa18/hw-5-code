const Item = require('../src/item.js').Item;
const GroceryList = require('../src/groceryList.js').GroceryList;
const assert = require('chai').assert;


describe('GroceryList', function() {
    let milk
    let bread
    let list;
    
    before(() => {
        milk = new Item('Milk', 2.50);
        bread = new Item('Bread', 2.00);
    });
        
    beforeEach(() => {
        list = new GroceryList();
        list.addItem(milk, 1);
    });
    /*
    SOLUTIONS:

    describe('#getList()', function() {
        it('returns an array', function(done) {
            assert.typeOf(list.getList(), 'array');
            done();
        });
        it('contains the correct items', function(done) {
            assert.isTrue(list.getList().includes(milk));
            assert.isFalse(list.getList().includes(bread));
            done();
        });
    });

    describe('#getListSize()', function() {
        it('returns a number', function(done) {
            assert.isNumber(list.getListSize());
            done();
        });
        it('returns the correct value', function(done) {
            assert.equal(list.getListSize(), 1);
            done();
        });
    });

    describe('#addItem()', function() {
        it('adds the item to the array', function(done) {
            assert.isFalse(list.getList().includes(bread));
            list.addItem(bread, 1);
            assert.isTrue(list.getList().includes(bread));
            done();
        });
        it('increases the size by one for each quantity', function(done) {
            let size = list.getListSize();
            list.addItem(bread, 3);
            assert.equal(list.getListSize(), size + 3);
            done();
        });
        it('does nothing if the quantity is 0', function(done) {
            let tempList = list.getList();
            list.addItem(bread, 0);
            assert.equal(list.getList(), tempList);
            done();
        });
        it('does nothing if the quantity is negative', function(done) {
            let tempList = list.getList();
            list.addItem(bread, -2);
            assert.equal(list.getList(), tempList);
            done();
        });
    });

    describe('#removeItem()', function() {
        it('removes an item from the list', function(done) {
            assert.isTrue(list.getList().includes(milk));  
            list.removeItem(milk);
            assert.isFalse(list.getList().includes(milk));            
            done();
        });
        it('removes one item from the list', function(done) {
            let size = list.getListSize();
            list.removeItem(milk);
            assert.equal(list.getListSize(), size - 1);
            done();           
        });
        it('removes only one copy of the item from the list if there are duplicates', function(done) {
            list.addItem(milk, 2);
            let size = list.getListSize();
            list.removeItem(milk);
            assert.equal(list.getListSize(), size - 1);
            assert.isTrue(list.getList().includes(milk));            
            done();
        });
        it('does nothing if the Item does not exist in the list', function(done) {
            let tempList = list.getList();
            list.removeItem(bread);
            assert.equal(list.getList(), tempList);
            done();
        });      
    });

    describe('#getListPrice()', function() {
        it('returns a number', function(done) {
            assert.isNumber(list.getListPrice());
            done();
        });
        it('returns the sum of all item prices', function(done) {
            list.addItem(bread, 2);
            list.addItem(milk, 3);
            assert.equal(list.getListPrice(), 14.00);
            done();
        });      
    });
    */
});