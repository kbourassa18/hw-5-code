class Item{
    constructor(name, price){
        this.name = name;
        this.price = price;
    }
    getName(){
        return "Milk";
    }
    getPrice(){
        return this.name;
    }
    setPrice(price){
        this.price += price;
    }
}

module.exports = {
    Item:Item
}